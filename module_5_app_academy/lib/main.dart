import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      // I will be using a Firebase database for the Edit User profile page to read, edit, add and delete user information from the firebase
      options: const FirebaseOptions(
          apiKey: "AIzaSyAcZ8fZoZ0g4Q24pIE2NDLuFtAXvCXIrGk",
          authDomain: "mtn-week5.firebaseapp.com",
          projectId: "mtn-week5",
          storageBucket: "mtn-week5.appspot.com",
          messagingSenderId: "600111091418",
          appId: "1:600111091418:web:bcca1c0c5b35045a329fe3"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Naqeeb Emeran - MTN App Academy Module 4 Assignment',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
                primarySwatch: Colors.amber) // Constant theme for app
            .copyWith(secondary: Colors.black),
      ),
      home: const MyHomePage(
          title: 'Naqeeb Emeran - MTN App Academy Module 4 Assignment'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SafeArea(
            child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height,
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            "Hello There!",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 40),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(
                            "Welcome to my MTN App Academy Module 4 Assignment!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.grey[700], fontSize: 15),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(30),
                            child: SizedBox(
                              height: 155.0,
                              child: Image.asset(
                                "assets/mtnbuslogo.jpg",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return const Login();
                              }));
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          const SizedBox(
                            height: 20.0,
                          ),
                          const Text("Don't have an account?"),
                          TextButton(
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return const Signup();
                                }));
                              },
                              child: const Text("Sign up"))
                        ],
                      ),
                    ]))));
  }
}

class Signup extends StatelessWidget {
  const Signup({Key? key}) : super(key: key);
  final String title = "Sign Up";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
                child: Image.asset(
                  "assets/mtnbuslogo.jpg",
                  fit: BoxFit.scaleDown,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: "Full Name",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: "Email",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: "Password",
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return const Dashboard();
                        }));
                      },
                      child: const Text(
                        "Sign up",
                        style: TextStyle(fontSize: 18),
                      )))
            ],
          ),
        ));
  }
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);
  final String title = "Login";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 40.0, bottom: 30.0, left: 150, right: 150),
              child: Image.asset(
                "assets/mtnbuslogo.jpg",
                fit: BoxFit.contain,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Email",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Password",
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Dashboard();
                      }));
                    },
                    child: const Text(
                      "Login",
                      style: TextStyle(fontSize: 18),
                    )))
          ],
        ),
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);
  final String title = "Dashboard";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("View Livestreams"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Vlives();
                      }));
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("View Gallery"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const Gallery();
                      }));
                    }),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("FAQs"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const FAQs();
                      }));
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).colorScheme.primary,
                    child: const Text("Edit User Profiles"),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return EditProf();
                      }));
                    }),
              )
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: MaterialButton(
                  height: 100.0,
                  minWidth: 150.0,
                  color: Theme.of(context).colorScheme.primary,
                  child: const Text("Logout"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const MyApp();
                    }));
                  }),
            ),
          ])
        ],
      )),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.computer_rounded),
        backgroundColor: Theme.of(context).colorScheme.primary,
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const Dashboard();
          }));
        },
      ),
    );
  }
}

class Vlives extends StatelessWidget {
  const Vlives({Key? key}) : super(key: key);
  final String title = "View Livestreams";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: SingleChildScrollView(
            child: Column(children: const <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 15.0),
            child: Text(
              "App Academy",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 5",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 24, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 4",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 17, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 3",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 10, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 2",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "May 3, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
            child: Text(
              "Week 1",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 10.0),
            child: Text(
              "View",
              style:
                  TextStyle(fontSize: 20, decoration: TextDecoration.underline),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 30.0),
            child: Text(
              "April 26, 2022 (10:00 - 12:30)",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ])),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

class Gallery extends StatelessWidget {
  const Gallery({Key? key}) : super(key: key);
  final String title = "Gallery";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(children: [
          Column(
            children: [
              Column(children: [
                SizedBox(
                  child: Image.asset(
                    "assets/Gallery.png",
                    fit: BoxFit.scaleDown,
                  ),
                )
              ])
            ],
          ),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

class FAQs extends StatelessWidget {
  const FAQs({Key? key}) : super(key: key);
  final String title = "FAQs";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(children: [
          Column(
            children: [
              Column(children: [
                SizedBox(
                  child: Image.asset(
                    "assets/FAQs.png",
                    fit: BoxFit.scaleDown,
                  ),
                )
              ])
            ],
          ),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.computer_rounded),
            backgroundColor: Theme.of(context).colorScheme.primary,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const Dashboard();
              }));
            }));
  }
}

// ignore: must_be_immutable
class EditProf extends StatelessWidget {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController dobController = TextEditingController();

  Future _editProfiles() {
    final name = nameController.text;
    final email = emailController.text;
    final password = passwordController.text;
    final dob = dobController.text;

    final ref = FirebaseFirestore.instance.collection("User Info").doc();

    return ref.set({
      "Full Name": name,
      "Email": email,
      "Password": password,
      "Date of Birth": dob,
      "doc_id": ref.id,
    }).then((value) => {
          nameController.text = "",
          emailController.text = "",
          passwordController.text = "",
          dobController.text = "",
        });
  }

  EditProf({Key? key}) : super(key: key);
  final String title = "Edit User Profiles";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
              child: Image.asset(
                "assets/user-profile.jpg",
                height: 215,
                width: 215,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Full Name",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Email",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                controller: passwordController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Password",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(300.0, 10.0, 300.0, 10.0),
              child: TextField(
                controller: dobController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Date of Birth (dd/mm/yy)",
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(200.0, 10.0, 200.0, 10.0),
                child: ElevatedButton(
                    onPressed: () {
                      _editProfiles();
                    },
                    child: const Text(
                      "Save Details",
                      style: TextStyle(fontSize: 18),
                    ))),
            const Padding(padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 30.0),
            child: Text("User Profiles",
            style:
                  TextStyle(fontSize: 40, 
                  decoration: TextDecoration.underline)),),
            const Admin()
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.computer_rounded),
        backgroundColor: Theme.of(context).colorScheme.primary,
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const Dashboard();
          }));
        },
      ),
    );
  }
}

class Admin extends StatefulWidget {
  const Admin({Key? key}) : super(key: key);
  final String title = "Admin";
  @override
  State<Admin> createState() => _AdminState();
}

class _AdminState extends State<Admin> {
  final Stream<QuerySnapshot> _users =
      FirebaseFirestore.instance.collection("User Info").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _nameFieldController = TextEditingController();
    TextEditingController _emailFieldController = TextEditingController();
    TextEditingController _passWrdFieldController = TextEditingController();
    TextEditingController _doBFieldController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance.collection("User Info").doc(docId).delete();
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("User Info");

      _nameFieldController.text = data["Full Name"];
      _emailFieldController.text = data["Email"];
      _passWrdFieldController.text = data["Password"];
      _doBFieldController.text = data["Date of Birth"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text("Update"),
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(
              controller: _nameFieldController,
            ),
            TextField(
              controller: _emailFieldController,
            ),
            TextField(
              controller: _passWrdFieldController,
            ),
            TextField(
              controller: _doBFieldController,
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
                child: OutlinedButton(
                    onPressed: () {
                      collection.doc(data["doc_id"]).update({
                        "Full Name": _nameFieldController.text,
                        "Email": _emailFieldController.text,
                        "Password": _passWrdFieldController.text,
                        "Date of Birth": _doBFieldController.text,
                      });
                      Navigator.pop(context);
                    },
                    child: const Text("Update User Data")))
          ]),
        ),
      );
    }

    return StreamBuilder(
        stream: _users,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return const Text("Something went wrong");
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          if (snapshot.hasData) {
            return Row(
              children: [
                Expanded(
                    child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.width),
                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data() as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(data["Full Name"]),
                                  subtitle: Text(data["Email"]),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      icon: const Icon(Icons.edit),
                                      label: const Text("Edit"),
                                    ),
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _delete(data["doc_id"]);
                                      },
                                      icon: const Icon(Icons.remove),
                                      label: const Text("Delete"),
                                    )
                                  ],
                                ))
                              ],
                            ),
                          )
                        ],
                      );
                    }).toList(),
                  ),
                ))
              ],
            );
          } else {
            return (const Text("No Data"));
          }
        });
  }
}
